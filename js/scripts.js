jQuery(document).ready(function($){
	
	$("#toggle-menu").click(function() {
		$('header').toggleClass('show-menu');
		$(this).toggleClass('close-me');
	});


	$(window).on("scroll", function() {
	    if($(window).scrollTop() > 100) {
	        $("header").addClass("highlight");
	    } else {        
	       $("header").removeClass("highlight");
	    }
	});

		
});

